module.exports = {
    testPathIgnorePatterns: ["/node_modules", "/cypress", "/cache"],
    modulePaths: ["<rootDir>src"],
    coverageDirectory: "<rootDir>results/coverage",
    moduleFileExtensions: ["js", "jsx", "json"],
    moduleDirectories: ["node_modules"],
}
