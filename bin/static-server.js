var compression = require("compression")
const express = require("express")
const path = require("path")
const app = express()
const port = process.env.PORT

const indexHtml = path.join(__dirname, "../dist/index.html")

app.use(compression())
app.use(express.static(path.join(__dirname, "../dist")))
app.use("*", (req, res) => res.sendFile(indexHtml))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
