describe("Show Drawer", function() {
    it("Should show when menu icon is clicked", function() {
        cy.viewport("iphone-6")
            .visit("/")
            .get("[data-cy=menuButton]")
            .click()
            .get("[data-cy=drawer]")
            .should("be.visible")
    })
    it("Should show the categories", function() {
        cy.viewport("iphone-6")
            .visit("/")
            .get("[data-cy=menuButton]")
            .click()
            .get("[data-cy=general]")
            .should("be.visible")
    })
    it("Should navigate to a category", function() {
        cy.viewport("iphone-6")
            .visit("/")
            .get("[data-cy=menuButton]")
            .click()
            .get("[data-cy=general]")
            .click()
            .url()
            .should("include", "/general")
    })
})
