describe("Home Button", function() {
    it("Should show the home button", function() {
        cy.viewport("iphone-6")
            .visit("/general")
            .get("[data-cy=homeButton]")
            .should("be.visible")
    })
    it("Should navigate to home", function() {
        cy.viewport("iphone-6")
            .visit("/general")
            .get("[data-cy=homeButton]")
            .click()
            .url()
            .should("eq", "http://localhost:8080/")
    })
})
