describe("Show Menu Icon", function() {
    it("Should not show if viewport is larger than 960px ", function() {
        cy.viewport(960, 800)
            .visit("/")
            .get("[data-cy=menuButton]")
            .should("not.be.visible")

        cy.viewport(1280, 800)
            .visit("/")
            .get("[data-cy=menuButton]")
            .should("not.be.visible")

        cy.viewport(1920, 800)
            .visit("/")
            .get("[data-cy=menuButton]")
            .should("not.be.visible")
    })
    it("Should show if viewport is smaller than 960px ", function() {
        cy.viewport(600, 800)
            .visit("/")
            .get("[data-cy=menuButton]")
            .should("be.visible")

        cy.viewport("iphone-6")
            .visit("/")
            .get("[data-cy=menuButton]")
            .should("be.visible")
    })
})
