describe("Navigate back", function() {
    it("Should go back to home page", function() {
        cy.visit("/")
            .get("[data-cy=general]")
            .click()
            .url()
            .should("include", "/general")
            .get("[data-cy=backButton]")
            .click()
            .url()
            .should("eq", "http://localhost:8080/")
    })
})
