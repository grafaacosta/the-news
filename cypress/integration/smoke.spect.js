describe("Smoke tests", function() {
    it("Home Page", function() {
        cy.visit("/")
    })
    it("General Page", function() {
        cy.visit("/general")
    })
    it("Entertainment Page", function() {
        cy.visit("/entertainment")
    })
    it("Health Page", function() {
        cy.visit("/health")
    })
    it("Science Page", function() {
        cy.visit("/science")
    })
    it("Sports Page", function() {
        cy.visit("/sports")
    })
    it("Technology Page", function() {
        cy.visit("/technology")
    })
})
