describe("Show Headline", function() {
    it("Should show at least one headline in entertainment", function() {
      cy.visit("/")
        .get("[data-cy=entertainment]")
        .click()
        .get("[data-cy=headline]")
        .its("length")
        .should("be.gt", 1)
    })
    it("Should show at least one headline in general", function() {
        cy.visit("/")
            .get("[data-cy=general]")
            .click()
            .get("[data-cy=headline]")
            .its("length")
            .should("be.gt", 1)
    })
    it("Should show at least one headline", function() {
        cy.visit("/")
            .get("[data-cy=headline]")
            .its("length")
            .should("be.gt", 1)
    })
})
