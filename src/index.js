import "./static"

import React from "react"
import ReactDOM from "react-dom"
import { createStore, applyMiddleware, compose } from "redux"
import createSagaMiddleware from "redux-saga"

import rootReducer from "App/reducer"
import rootSaga from "App/sagas"

import App from "App/View"

if ("serviceWorker" in navigator) {
    window.addEventListener("load", () => {
        navigator.serviceWorker
            .register("/sw.js")
            .then(registration => {
                console.log("SW registered: ", registration)
            })
            .catch(registrationError => {
                console.log("SW registration failed: ", registrationError)
            })
    })
}

const root = document.getElementById("root")

const iniState = window.BOOTSTRAP_CLIENT_STATE
const sagaMiddleware = createSagaMiddleware()

if (process.env.NODE_ENV === "production") {
    const store = createStore(
        rootReducer,
        applyMiddleware(sagaMiddleware),
        iniState
    )
    ReactDOM.render(<App store={store} />, root)
} else {
    const composeEnhancers =
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose // eslint-disable-line no-underscore-dangle
    const store = createStore(
        rootReducer,
        composeEnhancers(applyMiddleware(sagaMiddleware))
    )

    module.hot.accept("App/reducer", () => {
        store.replaceReducer(require("App/reducer"))
    })

    ReactDOM.render(<App store={store} />, root)
}

sagaMiddleware.run(rootSaga)
