import { makeQueryString } from "./index"

describe("## makeQueryString", () => {
    it("should transform an object into qs params", () => {
        const params = {
            key: "kdjfkdf",
            search: "some search",
        }
        const expected = "?key=kdjfkdf&search=some search"
        const actual = makeQueryString(params)
        expect(actual).toEqual(expected)
    })

    it("should return the empty string if no params", () => {
        const params = {}
        const expected = ""
        const actual = makeQueryString(params)
        expect(actual).toEqual(expected)
    })

    it("should return the empty string params is nil", () => {
        const expected = ""
        const actual = makeQueryString()
        expect(actual).toEqual(expected)
    })
})
