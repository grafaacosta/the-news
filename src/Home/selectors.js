import * as R from "ramda"
import { ns, iniState } from "Home/reducer"

const getNs = R.propOr(iniState, ns)

export const getHeadlines = R.compose(
    R.prop("headlines"),
    getNs
)

export const getIsImgLoaded = (state, props) => {
    const id = R.propOr("", "urlToImage", props)
    const loadedImages = R.propOr([], "loadedImages", getNs(state))

    return R.contains(id, loadedImages)
}
