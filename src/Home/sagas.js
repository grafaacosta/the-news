import { fork, call, put, select, takeEvery, all } from "redux-saga/effects"

import { headlinesResponse } from "Home/reducer"

import { fetchNews } from "App/sagas/newsApi"
import { fetchImages } from "App/sagas/imagesApi"
import { getCountry } from "App/selectors"

import { SET_CATEGORY, LOAD_IMAGE } from "Home/reducer"

const fetchHeadlines = function*({ payload: category }) {
    try {
        const country = yield select(getCountry)
        const headlines = yield call(fetchNews, {
            url: "/top-headlines",
            qs: { country, category },
        })
        yield put(headlinesResponse(headlines))
    } catch (e) {
        yield put(headlinesResponse(null, {}, true))
    }
}

const watchSetCategory = function*() {
    yield takeEvery(SET_CATEGORY, fetchHeadlines)
}

const loadImage = function*({ paylaod: urlToImage }) {
    if (urlToImage) {
        yield call(fetchImages, { url: urlToImage })
    }
}

const watchFetchImages = function*() {
    yield takeEvery(LOAD_IMAGE, loadImage)
}

const rootSaga = function*() {
    yield call(fetchHeadlines, { payload: "" })
    yield all([fork(watchSetCategory), fork(watchFetchImages)])
}

export default rootSaga
