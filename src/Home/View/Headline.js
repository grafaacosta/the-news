import * as R from "ramda"
import { connect } from "react-redux"
import React, { useState, useRef, useCallback, useEffect } from "react"

import { loadImage } from "Home/reducer"
import { getIsImgLoaded } from "Home/selectors"

import { makeStyles } from "@material-ui/styles"
import Link from "@material-ui/core/Link"
import Paper from "@material-ui/core/Paper"
import Box from "@material-ui/core/Box"
import Typography from "@material-ui/core/Typography"
import Collapse from "@material-ui/core/Collapse"
import CardMedia from "@material-ui/core/CardMedia"

import { noop } from "utils"

const useStyles = makeStyles(theme => ({
    description: {
        marginTop: theme.spacing(2),
    },
    media: {
        marginTop: 20,
        height: 0,
        paddingTop: "56.25%", // 16:9
    },
    headlineContainer: {
        paddingLeft: theme.spacing(4),
        paddingRight: theme.spacing(8),
        paddingTop: theme.spacing(6),
        paddingBottom: theme.spacing(6),
        cursor: "pointer",
    },
}))

const decorate = connect(
    (state, props) => ({
        isImgLoaded: getIsImgLoaded(state, props),
    }),
    {
        loadImage,
    }
)

const isInViewport = elm => {
    const bounding = elm.getBoundingClientRect()
    return (
        bounding.top <=
        (window.innerHeight || document.documentElement.clientHeight) + 10
    )
}

const Headline = ({
    title = "",
    description = "",
    url = "",
    urlToImage = "",
    isImgLoaded = false,
    isMobile = false,
    loadImage = noop,
}) => {
    const classes = useStyles()
    const [expanded, setExpanded] = useState(false)
    const headlineRef = useRef(null)

    const inViewport = useCallback(() => {
        if (isInViewport(headlineRef.current)) {
            loadImage(urlToImage)
        }
    }, [headlineRef.current, urlToImage, isMobile])

    useEffect(() => {
        if (!isImgLoaded) {
            inViewport()
            window.addEventListener("scroll", inViewport)
            return () => {
                window.removeEventListener("scroll", inViewport)
            }
        }
        return noop
    }, [inViewport, isImgLoaded])

    const content = (
        <>
            <Typography variant="h4">{title}</Typography>
            <Collapse
                in={isMobile || expanded}
                timeout="auto"
                collapsedHeight="50px"
            >
                <Typography
                    variant="h6"
                    component={Box}
                    className={classes.description}
                    onMouseEnter={() => setExpanded(true)}
                >
                    {description}
                </Typography>
                {!(
                    R.isNil(urlToImage) ||
                    R.isEmpty(urlToImage) ||
                    !isImgLoaded
                ) ? (
                    <CardMedia className={classes.media} image={urlToImage} />
                ) : null}
            </Collapse>
        </>
    )
    return (
        <Paper
            className={classes.headlineContainer}
            onMouseLeave={() => setExpanded(false)}
            data-cy="headline"
            ref={headlineRef}
        >
            {R.or(R.isNil(url), R.isEmpty(url)) ? (
                content
            ) : (
                <Link
                    href={url}
                    underline="none"
                    target="_blank"
                    rel="noopener"
                >
                    {content}
                </Link>
            )}
        </Paper>
    )
}

export default decorate(Headline)
