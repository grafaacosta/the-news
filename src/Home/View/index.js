import React, { useEffect } from "react"
import { connect } from "react-redux"
import { noop } from "utils"

import { setCategory } from "Home/reducer"

import { getIsMobile } from "App/selectors"
import { getHeadlines } from "Home/selectors"

import { makeStyles } from "@material-ui/styles"
import Grid from "@material-ui/core/Grid"

import Headline from "./Headline"

const useStyles = makeStyles(theme => ({
    pageContainer: {
        marginTop: 70,
        width: "100%",
        padding: "20px",
    },
    headlineContainer: {
        paddingLeft: theme.spacing(4),
        paddingRight: theme.spacing(8),
        paddingTop: theme.spacing(6),
        paddingBottom: theme.spacing(6),
        cursor: "pointer",
    },
    description: {
        marginTop: theme.spacing(2),
    },
    media: {
        marginTop: 20,
        height: 0,
        paddingTop: "56.25%", // 16:9
    },
}))

const decorate = connect(
    (state, props) => ({
        headlines: getHeadlines(state, props),
        isMobile: getIsMobile(state, props),
    }),
    {
        setCategory,
    }
)

const Home = ({
    headlines = [],
    categoryId,
    isMobile = false,
    setCategory = noop,
}) => {
    useEffect(() => {
        setCategory(categoryId)
    }, [categoryId])
    const classes = useStyles()

    return (
        <div className={classes.pageContainer}>
            <Grid container spacing={2} direction="row">
                {headlines.map((headline, index) => (
                    <Grid
                        item
                        xs={12}
                        sm={12}
                        md={6}
                        lg={6}
                        xl={4}
                        key={index} // the api does not have a unique id
                    >
                        <Headline {...headline} isMobile={isMobile} />
                    </Grid>
                ))}
            </Grid>
        </div>
    )
}

export default decorate(Home)
