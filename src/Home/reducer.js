import * as R from "ramda"
import { makeReducer, makeFSA } from "utils"

export const ns = "Home"

export const iniState = {
    headlines: [],
    loading: false,
    activeCategory: "",
    loadedImages: [],
}

const HEADLINES_REQUEST = `${ns}/headlinesRequest`
export const headlinesRequest = makeFSA(HEADLINES_REQUEST)

const HEADLINES_RESPONSE = `${ns}/headlinesResponse`
export const headlinesResponse = makeFSA(HEADLINES_RESPONSE)

export const SET_CATEGORY = `${ns}/setCategory`
export const setCategory = makeFSA(SET_CATEGORY)

export const LOAD_IMAGE = `${ns}/loadImg`
export const loadImage = makeFSA(LOAD_IMAGE)

const reducers = {
    [SET_CATEGORY]: (_, { payload: activeCategory }) => ({
        activeCategory,
        loadedImages: [],
    }),
    [HEADLINES_RESPONSE]: (_, { payload: { articles: headlines }, error }) =>
        error ? { loading: false } : { headlines, loading: false },
    [LOAD_IMAGE]: ({ loadedImages }, { payload: id }) => ({
        loadedImages: R.append(id, loadedImages),
    }),
}

export default makeReducer(reducers, iniState)
