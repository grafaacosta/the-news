/* eslint-disable */
workbox.precaching.precacheAndRoute(self.__precacheManifest)

// runtime cache
// 1. stylesheet
workbox.routing.registerRoute(
    new RegExp(".css$"),
    new workbox.strategies.CacheFirst({
        cacheName: "css-cache",
        plugins: [
            new workbox.expiration.Plugin({
                maxAgeSeconds: 60 * 60 * 24 * 7,
                maxEntries: 20,
                purgeOnQuotaError: true,
            }),
        ],
    })
)
// 2. images
workbox.routing.registerRoute(
    new RegExp(".(png|svg|jpg|jpeg)$"),
    new workbox.strategies.CacheFirst({
        cacheName: "images-cache",
        plugins: [
            new workbox.expiration.Plugin({
                maxAgeSeconds: 60 * 60 * 24 * 7,
                maxEntries: 50,
                purgeOnQuotaError: true,
            }),
        ],
    })
)

// 3. cache news articles result
workbox.routing.registerRoute(
    /https:\/\/newsapi\.org\/v2\/top-headlines/,
    new workbox.strategies.NetworkFirst({
        cacheName: "routes-cache",
        plugins: [
            new workbox.expiration.Plugin({
                maxAgeSeconds: 10 * 60,
            }),
        ],
    })
)
