import * as R from "ramda"
import { ns, iniState } from "App/reducer/config"

const getNs = R.propOr(iniState, ns)

export const getNewsToken = R.compose(
    R.prop("newsToken"),
    getNs
)

export const getNewsUrl = R.compose(
    R.prop("newsUrl"),
    getNs
)

export const getCountry = R.compose(
    R.prop("country"),
    getNs
)

export const getShowDrawer = R.compose(
    R.prop("showDrawer"),
    getNs
)

export const getIsMobile = R.compose(
    R.prop("isMobile"),
    getNs
)
