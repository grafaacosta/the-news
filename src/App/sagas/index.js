import { fork, put } from "redux-saga/effects"

import home from "Home/sagas"
import { setIsMobile } from "App/reducer/config"

import { isMobile } from "utils"

const rootSaga = function*() {
    try {
        if (isMobile()) {
            yield put(setIsMobile(true))
        }
    } catch (e) {
        yield put(setIsMobile(false))
    }
    yield fork(home)
}

export default rootSaga
