import { call } from "redux-saga/effects"

export const fetchImages = function*({ url, ...params }) {
    const response = yield call(fetch, url, params)
    if (!response.ok) {
        throw new Error(response)
    }
}
