import { call, select } from "redux-saga/effects"

import { getNewsToken, getNewsUrl } from "App/selectors"
import { makeQueryString } from "utils"

const parseResponse = function*(response) {
    return yield call([response, response.json])
}

export const fetchNews = function*(options) {
    const apiKey = yield select(getNewsToken)
    const baseUrl = yield select(getNewsUrl)
    const { qs, ...params } = options
    const query = makeQueryString({ ...qs, apiKey })

    const response = yield call(
        fetch,
        `${baseUrl}${options.url}${query}`,
        params
    )
    if (response.ok) {
        return yield call(parseResponse, response)
    }

    throw new Error(response)
}
