import * as R from "ramda"
import React from "react"
import { Link } from "@reach/router"
import { connect } from "react-redux"
import { noop } from "utils"

import { getShowDrawer } from "App/selectors"
import { toggleDrawer } from "App/reducer/config"

import { makeStyles } from "@material-ui/core/styles"
import Drawer from "@material-ui/core/Drawer"
import Box from "@material-ui/core/Box"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import Divider from "@material-ui/core/Divider"

import { CATEGORIES } from "App/constants"

const useStyles = makeStyles(theme => ({
    container: {
        backgroundColor: theme.palette.primary.dark,
        display: "flex",
        flexDirection: "column",
        height: "100%",
        color: theme.palette.primary.contrastText,
        fontWeight: 600,
        width: 200,
    },
}))

const decorate = connect(
    (state, props) => ({
        open: getShowDrawer(state, props),
    }),
    {
        onClose: toggleDrawer,
    }
)

const LeftDrawer = ({ open = true, onClose = noop }) => {
    const classes = useStyles()
    return (
        <Drawer anchor="left" open={open} onClose={onClose} data-cy="drawer">
            <Box className={classes.container}>
                <List>
                    {R.map(
                        category => (
                            <Link
                                to={`/${category}`}
                                onClick={onClose}
                                key={category}
                            >
                                <ListItem
                                    button
                                    key={category}
                                    data-cy={category}
                                >
                                    {R.toUpper(category)}
                                </ListItem>
                            </Link>
                        ),
                        CATEGORIES
                    )}
                    <Divider />
                </List>
            </Box>
        </Drawer>
    )
}

export default decorate(LeftDrawer)
