import * as R from "ramda"
import React from "react"

import { makeStyles } from "@material-ui/core/styles"
import Button from "@material-ui/core/Button"
import { Link } from "@reach/router"

const getIsDisabled = (location, category) => {
    if (location.pathname === "/") {
        return false
    }
    return R.tail(location.pathname) !== category
}

const useStyles = makeStyles({
    categoryButton: {
        fontWeight: 600,
    },
})

const Category = ({ category, location }) => {
    const classes = useStyles()
    const isDisabled = getIsDisabled(location, category)
    const content = (
        <Button
            color="inherit"
            key={category}
            disabled={isDisabled}
            className={classes.categoryButton}
            data-cy={category}
        >
            {category}
        </Button>
    )
    return isDisabled ? content : <Link to={`/${category}`}>{content}</Link>
}

export default Category
