import * as R from "ramda"
import React, { Component } from "react"
import { noop } from "utils"
import { connect } from "react-redux"
import { Link } from "@reach/router"

import { toggleDrawer } from "App/reducer/config"

import { makeStyles, useTheme } from "@material-ui/core/styles"
import useMediaQuery from "@material-ui/core/useMediaQuery"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import IconButton from "@material-ui/core/IconButton"
import MenuIcon from "@material-ui/icons/Menu"
import BackIcon from "@material-ui/icons/ArrowBack"
import HomeIcon from "@material-ui/icons/Home"
import Button from "@material-ui/core/Button"
import { Location } from "@reach/router"

import Category from "./Category"

import { CATEGORIES } from "App/constants"

const showMenuButton = (location, smallViewport) => {
    if (smallViewport) {
        return true
    }
    return location.pathname !== "/"
}

const showBackIcon = (location, smallViewport) => {
    if (smallViewport) {
        return false
    }
    return location.pathname !== "/"
}

const useStyles = makeStyles(theme => ({
    menuButton: {
        marginRight: theme.spacing(2),
    },
    menuButtonHidden: {
        marginRight: theme.spacing(2),
        visibility: "hidden",
    },
    categoryButton: {
        fontWeight: 600,
    },
}))

const decorate = connect(
    null,
    {
        onMenu: toggleDrawer,
    }
)

const TopBarFn = decorate(({ onMenu = noop }) => {
    const classes = useStyles()
    const theme = useTheme()
    const bigViewport = useMediaQuery(theme.breakpoints.up("md"))

    return (
        <Location>
            {({ location, navigate }) => (
                <AppBar>
                    <Toolbar>
                        <IconButton
                            edge="start"
                            className={
                                showMenuButton(location, !bigViewport)
                                    ? classes.menuButton
                                    : classes.menuButtonHidden
                            }
                            color="inherit"
                            aria-label="menu"
                            onClick={
                                showBackIcon(location, !bigViewport)
                                    ? () => navigate("/")
                                    : onMenu
                            }
                        >
                            {showBackIcon(location, !bigViewport) ? (
                                <BackIcon data-cy="backButton" />
                            ) : (
                                <MenuIcon data-cy="menuButton" />
                            )}
                        </IconButton>
                        {(bigViewport &&
                            R.map(
                                category => (
                                    <Category
                                        key={category}
                                        category={category}
                                        location={location}
                                    />
                                ),
                                CATEGORIES
                            )) ||
                            (location.pathname !== "/" ? (
                                <>
                                    <Link to="/">
                                        <IconButton
                                            className={classes.menuButton}
                                            color="inherit"
                                            aria-label="home"
                                        >
                                            <HomeIcon data-cy="homeButton" />
                                        </IconButton>
                                    </Link>
                                    <Category
                                        category={R.tail(location.pathname)}
                                        location={location}
                                    />
                                </>
                            ) : (
                                <Button
                                    color="inherit"
                                    className={classes.categoryButton}
                                >
                                    THE NEWS
                                </Button>
                            ))}
                    </Toolbar>
                </AppBar>
            )}
        </Location>
    )
})

class TopBar extends Component {
    render() {
        return <TopBarFn {...this.props} />
    }
}

export default TopBar
