import React from "react"
import { Provider } from "react-redux"
import { hot } from "react-hot-loader"

import { createMuiTheme } from "@material-ui/core/styles"
import { ThemeProvider as MaterialUIThemeProvider } from "@material-ui/styles"

import Layout from "./Layout"

const theme = createMuiTheme({
    palette: {
        primary: {
            light: "#3c4350",
            main: "#161c28",
            dark: "#000000",
            contrastText: "white",
        },
    },
    typography: {
        fontFamily: ["Playfair Display", "serif"].join(","),
    },
})

const App = ({ store }) => (
    <MaterialUIThemeProvider theme={theme}>
        <Provider store={store}>
            <Layout />
        </Provider>
    </MaterialUIThemeProvider>
)

export default hot(module)(App)
