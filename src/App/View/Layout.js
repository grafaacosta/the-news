import React from "react"
import { Router } from "@reach/router"

import TopBar from "./TopBar"
import HideOnScroll from "./HideOnScroll"
import LeftDrawer from "./LeftDrawer"
import Home from "Home/View"

import CssBaseline from "@material-ui/core/CssBaseline"

const Layout = () => (
    <>
        <CssBaseline />
        <HideOnScroll>
            <TopBar />
        </HideOnScroll>
        <LeftDrawer />
        <Router>
            <Home path="/" />
            <Home path="/:categoryId" />
        </Router>
    </>
)

export default Layout
