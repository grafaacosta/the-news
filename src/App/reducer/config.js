import { makeReducer, makeFSA, makeSetReducer } from "utils"

export const ns = "Config"

export const iniState = {
    newsToken: "3e1eb26fb911433d914cb9c5a9f365e4",
    newsUrl: "https://newsapi.org/v2",
    country: "us",
    showDrawer: false,
    isMobile: false,
}

const SET_NEWS_TOKEN = `${ns}/setNewsToken`
export const setNewsToken = makeFSA(SET_NEWS_TOKEN)

const SET_NEWS_URL = `${ns}/setNewsUrl`
export const setNewsUrl = makeFSA(SET_NEWS_URL)

const SET_COUNTRY = `${ns}/setCountry`
export const setCountry = makeFSA(SET_COUNTRY)

const TOGGLE_DRAWER = `${ns}/toggleDrawer`
export const toggleDrawer = makeFSA(TOGGLE_DRAWER)

const SET_IS_MOBILE = `${ns}/setIsMobile`
export const setIsMobile = makeFSA(SET_IS_MOBILE)

const reducers = {
    [SET_NEWS_TOKEN]: makeSetReducer("newsToken"),
    [SET_NEWS_URL]: makeSetReducer("newsUrl"),
    [SET_COUNTRY]: makeSetReducer("country"),
    [SET_IS_MOBILE]: makeSetReducer("isMobile"),
    [TOGGLE_DRAWER]: ({ showDrawer }) => ({ showDrawer: !showDrawer }),
}

export default makeReducer(reducers, iniState)
