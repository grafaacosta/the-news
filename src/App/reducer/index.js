import { combineReducers } from "redux"

import config, { ns as configNs } from "./config"
import home, { ns as homeNs } from "Home/reducer"

export default combineReducers({
    [configNs]: config,
    [homeNs]: home,
})
